package com.example.dep;

/**
 * Hello name!
 *
 */
public class Dep
{
    public static void hello( String name )
    {
        System.out.println( "Hello " + name + "!" );

       	int number = 10;
   	
        if (number > 1) {
    		System.out.println("Number is positive.");
    	} else
        if (number < -1) {
    		System.out.println("Number is negative.");
    	} else
        if (number > 0) {
    		System.out.println("Number is zero.");
    	} 


    }
}
